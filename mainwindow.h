#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл mainwindow.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или изменять
 * на условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения; либо версии 3 лицензии,
 * либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в надежде, что они будут
 * полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * mainwindow.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with each of
 * reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <QMainWindow>
#include "list.h"

QT_BEGIN_NAMESPACE
namespace Ui { class CMainWindow; }
QT_END_NAMESPACE

namespace ui {

class CMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	CMainWindow(QWidget *parent = nullptr);
	~CMainWindow();

	CList* FindList();

	enum {
		invalid,
		valid
	} m_state;

	bool IsValid() {
		return (m_state == valid);
    }

	// QObject interface
public:
	virtual bool event(QEvent *event);

private slots:
	void OnExit();
    void on_New_triggered();
    void on_Edit_triggered();
    void on_Del_triggered();
	void on_Rem_triggered();
	void on_action_triggered();

private:
	Ui::CMainWindow *ui;
};

}// namespace ui