/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл main.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или изменять
 * на условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения; либо версии 3 лицензии,
 * либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в надежде, что они будут
 * полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * main.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with each of
 * reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "mainwindow.h"
#include <QApplication>
//#include "../../common/io_strings.h"
#include "../../common/exception.h"
#include "global.h"
#include "../../common/ErrLog.h"
#include <csignal>

namespace glb {
    extern thread_local CFxString sAppPath;// путь к файлам приложения определяется при запуске
	extern QApplication *g_pa;
	extern EInit g_init;
	void on_abort(int);
}
/*namespace fl {
    extern CStrings strs;
}*/
namespace err {
	extern CLog log;
}

int main(int argc, char *argv[])
{
    try {
		QApplication a(argc, argv);
		glb::g_pa = &a;// для CMainWindow::OnExit...
		if (!setlocale(LC_ALL, ""))	// "ru_RU.UTF-8"; см. glb::cmp_iUtf8s()
			return 1;
		signal(SIGABRT, glb::on_abort);
		signal(SIGFPE, glb::on_abort);
		signal(SIGILL, glb::on_abort);
		signal(SIGINT, glb::on_abort);
		signal(SIGSEGV, glb::on_abort);
		signal(SIGTERM, glb::on_abort);
		ui::CMainWindow w;
		if (w.IsValid()) {
			w.show();
			return a.exec();
		}
	} catch (err::CLocalException e) {
		err::log.Add("%t местное исключение %d в main()", time(nullptr), e.m_iStrID);
    } catch (err::CCException e) {
		err::log.Add(S_IE, time(nullptr), e.text(), e.m_errno, __FILE__, __LINE__);
    } catch (...) {
		err::log.Add(S_IEU, time(nullptr), __FILE__, __LINE__);
    }
    return -1;
}
