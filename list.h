#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл list.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или изменять
 * на условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения; либо версии 3 лицензии,
 * либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в надежде, что они будут
 * полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * list.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with each of
 * reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include <QTableWidget>
#include "../../common/io_string.h"

namespace ui {

class CList : public QTableWidget
{
	Q_OBJECT

	uint32_t m_state;
	QString m_sEditItem;
	QFont m_font;

public:
	explicit CList(QWidget *parent = Q_NULLPTR);
	CList(int rows, int columns, QWidget *parent = Q_NULLPTR);

protected:
	void CloseEditor();
	QLineEdit* FindEdit();

	void q2s(const QString &qs, glb::CFxArray<BYTE, true> &to);
	int32_t BaseIndexCI() const;
	QBrush TextBrush(uint32_t uf) const;

	void AddNewRow(const fl::CString &s);
	void AddNewRow(int32_t iInd, int32_t iItem);
	QTableWidgetItem* InsertNewRow(int iRow);
	void SetRow(int32_t iRow, int32_t iInd, int32_t iItem);
	void SetRow(int32_t iRow, fl::CString &s);

public:
    void OnEdit();	// см. CMainWindow::OnEdit
    void OnNew();	// см. CMainWindow::OnNew
    void OnDel();	// см. CMainWindow::OnDel
	void OnRem();	// см. CMainWindow::OnRem
	void OnReadTextFile();

	void DelRow(int32_t iRow);
	void Empty();

public slots:
	void OnHorisontalHeaderClicked(int);

	// QObject interface
public:
	virtual bool event(QEvent *event);

	// QWidget interface
protected:
	virtual void focusInEvent(QFocusEvent *event);
	virtual void keyPressEvent(QKeyEvent *event);
	virtual void resizeEvent(QResizeEvent *event);
};

}// namespace ui