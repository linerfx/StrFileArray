/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл list.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или изменять
 * на условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения; либо версии 3 лицензии,
 * либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в надежде, что они будут
 * полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * list.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with each of
 * reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "list.h"
#include <QKeyEvent>
#include <QLineEdit>
#include <QHeaderView>
#include <QScrollBar>
#include <QMessageBox>
#include <QFileDialog>
#include "../../common/exception.h"
#include "../../common/ErrLog.h"
#include "strdef.h"
#include "../../common/io_strings.h"
#include <QApplication>

#define NONE	0
#define ACTIVE	0x00000001
// иницыацыя пройдена
#define INIT	0x00000002
// редактирование имеющегося текста
#define EDIT	0x00000004
// добавление новой строки
#define ADD		0x00000008
// список отсортирован по возрастанию
#define SORTASC	0x00000010
// список отсортирован по убыванию
#define SORTDES	0x00000020
// внутри ф-ции CloseEditor
#define INCLOSE	0x00000040

namespace glb {
    extern thread_local CFxString sAppPath;
}
namespace fl {
    extern CStrings strs;
}
namespace err {
	extern CLog log;
}
namespace ui {

CList::CList(QWidget *parent)
	: QTableWidget(parent)
{
	m_state = NONE;
	m_font.setStyleHint(QFont::Monospace);
	m_font.setFamily("Consolas");
	m_font.setWeight(QFont::Normal);
	m_font.setPointSize(10);
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectRows);
}

CList::CList(int rows, int columns, QWidget *parent)
	: QTableWidget(rows, columns, parent)
{
	m_state = NONE;
	m_font.setStyleHint(QFont::Monospace);
	m_font.setFamily("Consolas");
	m_font.setWeight(QFont::Normal);
	m_font.setPointSize(10);
	setSelectionMode(QAbstractItemView::SingleSelection);
	setSelectionBehavior(QAbstractItemView::SelectRows);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::OnHorisontalHeaderClicked п-ль клацнул по заголовку столбца... сортировка. Судя по
/// тому что пишут в справке, тут 3 состояния: "нету", "вниз", "вверх"; и так по кругу
/// Добавил возможность адаптироваться под изменения извне. Желательно тут конечно сделать уведомления
/// об изменении массива из др.приложений, пока реагирую пост-фактум.
/// \param не используется т.к. столбец всего один
///
void CList::OnHorisontalHeaderClicked(int /*iHeader*/)
{
	setCursor(Qt::WaitCursor);
	QHeaderView *ph = horizontalHeader();
	if (!ph->isSortIndicatorShown())
		ph->setSortIndicatorShown(true);
	QTableWidgetItem *pi;
	fl::a::COpener<fl::CStrings> opnr(fl::strs);
	int32_t i, cnt = fl::strs.Count(), nRows = rowCount();
	switch (m_state & (SORTASC|SORTDES)) {
	case 0:			// сначала сортирую по возрастанию
	case SORTDES:	// а если список отсортирован по убыванию, сортирую по возрастанию
		for (i = 0; i < cnt; i++)
			if (i < nRows)
				SetRow(i, fl::CString::iStrsLeti, i);
			else
				AddNewRow(fl::CString::iStrsLeti, i);
		m_state |= SORTASC; // первый флаг, если сортировки не было, второй, если была по убыванию
		break;
	case SORTASC:	// если список отсортирован по возрастанию...
		for (i = 0; i < cnt; i++)
			if (i < nRows)
				SetRow(i, fl::CString::iStrsLeti, cnt - i - 1);	// сортирую по убыванию
			else
				AddNewRow(fl::CString::iStrsLeti, cnt - i - 1);
		m_state |= SORTDES;	// добавляю второй флаг
		break;
	default:		// оба флага. Восстанавливаю по файловому отступу
		for (i = 0; i < cnt; i++)
			if (i < nRows)
				SetRow(i, fl::ioffset, i);
			else
				AddNewRow(fl::ioffset, i);
		m_state &= ~(SORTASC|SORTDES); // снимаю все флаги
		ph->setSortIndicatorShown(false);
		break;
	}
	while (i < cnt)
		DelRow(i++);
	unsetCursor();
}

void CList::CloseEditor()
{
	ASSERT(currentIndex().isValid());
	QTableWidgetItem *pi = currentItem();
	if (isPersistentEditorOpen(pi)) {
		m_state |= INCLOSE;
		closePersistentEditor(pi);
		m_state &= ~INCLOSE;
	}
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CMainWindow::FindEdit находит текстовый редактор, открытый ф-цией
/// QTableWidget::openPersistentEditor().
/// \return указатель на текстовый редактор
///
QLineEdit* CList::FindEdit()
{
	QList<QWidget*> ac(findChildren<QWidget*>());
	QWidget *pw;
	for (int i = 0; i < ac.count(); i++) {
		pw = ac[i];
/*		s = QString::asprintf("имя: \"%s\"; класс: \"%s\".", qPrintable(pw->objectName()),
				  qPrintable(pw->metaObject()->className()));
		pv->addItem(s);*/// QExpandingLineEdit
		if (pw->metaObject()->className() == QString("QExpandingLineEdit")) {
			QLineEdit *pe = dynamic_cast<QLineEdit*>(pw);
			ASSERT(pe->metaObject()->inherits(pw->metaObject()));
			return pe;
		}
	}
	throw err::CLocalException(SI_NED);
}

// вспомогательная ф-ция, её нужно вывести в qtcmn.cpp или вроде того
void CList::q2s(const QString &qs, glb::CFxArray<BYTE, true> &to)
{
	QByteArray qa = qs.toUtf8();
    int32_t cnt = qa.length();
	to.SetCountND(cnt);
	to.Edit(0, (const BYTE*) qa.data(), cnt);
}

//////////////////////////////////////////////////////////////////////////////////
/// \brief CList::BaseIndex
/// \return базовый индекс активного эл-та (current item)
///
int32_t CList::BaseIndexCI() const
{
	const int32_t c = currentRow();
	if (c < 0 || c >= rowCount())
		return -1;
	if (!(m_state & (SORTASC|SORTDES)))
		return c;
	QTableWidgetItem *pi = verticalHeaderItem(c);
	return (pi->text().toInt());
}

QBrush CList::TextBrush(uint32_t uf) const
{
	return QApplication::palette().brush((uf & ITM_DEL) ? QPalette::Disabled : QPalette::Active, QPalette::WindowText);
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief CList::AddNewRow добавляю в представление новую строку
/// \param s: добавляемая строка
///
void CList::AddNewRow(const fl::CString &s)
{
	int32_t iRow = rowCount();
	insertRow(iRow);
	QTableWidgetItem *pi = new QTableWidgetItem(tr("%1").arg(iRow));
	pi->setForeground(TextBrush(s.m_flags));
	setVerticalHeaderItem(iRow, pi);
	pi = new QTableWidgetItem(QString::fromUtf8(s.str()));
	pi->setFont(m_font);
	pi->setForeground(TextBrush(s.m_flags));
	setItem(iRow, 0, pi);
}

void CList::AddNewRow(int32_t iInd, int32_t iItem)
{
	ASSERT(fl::strs.IsOpen());
	fl::CString *ps = fl::strs.static_obj(NULL);
	AddNewRow(fl::strs.GetItem(iInd, iItem, *ps));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::InsertNewRow вставит строку в указанный индекс
/// \param iRow
/// \return
///
QTableWidgetItem* CList::InsertNewRow(int iRow)
{
	insertRow(iRow);
	setVerticalHeaderItem(iRow, new QTableWidgetItem(QString::number(iRow)));
	// поправлю ширину столбца, если нужно
	QHeaderView *phh = horizontalHeader(), *pvh = verticalHeader();
	QScrollBar *psb = verticalScrollBar();
	int nw = width(), nh = pvh->width(), nsb = psb->width();
	if (nsb > nh * 3)
		nsb = 0;
	nw -= nh + nsb;
	if (phh->sectionSize(0) != nw)
		phh->resizeSection(0, nw);
	// эл-т и редактор
	QTableWidgetItem *pi = new QTableWidgetItem;
	pi->setFont(m_font);
	setItem(iRow, 0, pi);
	return pi;
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::SetRow изменяет строку
/// \param iRow: индекс строки в представлении
/// \param iInd: индекс таблицы
/// \param iItem: эл-т таблицы
///
void CList::SetRow(int32_t iRow, int32_t iInd, int32_t iItem)
{
	fl::CString *ps = fl::strs.static_obj(NULL);
	QTableWidgetItem *pi = verticalHeaderItem(iRow);
	fl::strs.GetItem(iInd, iItem, *ps);
	pi->setForeground(TextBrush(ps->m_flags));
	pi->setText(QString::number((iInd == fl::ioffset) ? iItem : fl::strs.BaseIndex(iInd, iItem)));
	pi = item(iRow, 0);
	pi->setForeground(TextBrush(ps->m_flags));
	pi->setText(ps->str());
}

void CList::SetRow(int32_t iRow, fl::CString &s)
{
	QTableWidgetItem *pi = verticalHeaderItem(iRow);
	pi->setForeground(TextBrush(s.m_flags));
	pi->setText(QString::number(iRow));
	pi = item(iRow, 0);
	pi->setForeground(TextBrush(s.m_flags));
	pi->setText(s.str());
}

///////////////////////////////////////////////////////////////////////////////////
/// \brief CList::OnEdit редактирование п-лем строки списка. Открывает текстовый
/// редактор (который сам берёт текст из списка) и фокусируется на нём.
///
void CList::OnEdit()
{
	if (!currentIndex().isValid())
		return;
	QTableWidgetItem *pi = currentItem();
	m_sEditItem = pi->text();
	openPersistentEditor(pi);
	QLineEdit *le = FindEdit();
	le->setFont(m_font);
	le->setFocus(Qt::PopupFocusReason);
	m_state |= EDIT;
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::OnNew команда меню - доабвляю новую строку.
///
void CList::OnNew()
{
	ASSERT(rowCount() == fl::strs.Count());
	// теперь 17.10.2022 добавление может стать редактированием, если в базе есть
	// не действительные строки.
	typedef fl::a::CvShrd<fl::CString, fl::CString::EStrIndices::iStrsInds> STRS;
	const STRS &strs = fl::strs;
	const STRS::INDEX *pind = strs.GetIndex();
	int32_t iBas = -1;
	if (pind->Del()) {
		iBas = pind->Bas(fl::idel, 0);
		// если список отсортирован, восстановлю по ид-ру, чтобы ид-р стал индексом строки
		if (m_state & (SORTASC|SORTDES)) {
			m_state |= SORTASC|SORTDES;// сортирую по файловому отступу (по-умолчанию)
			OnHorisontalHeaderClicked(0);
		}// выделю не действительный эл-т списка
		if (!currentIndex().isValid() || currentIndex().row() != iBas) {
			setCurrentItem(item(iBas, 0), QItemSelectionModel::ClearAndSelect);
			scrollToItem(currentItem(), QAbstractItemView::PositionAtCenter);
		}// редактировать не действительный эл-т списка, не добавлять новый
		OnEdit();
		return;
	}// если индекс уникальный и строка в редакторе будет повторять имеющуюся, нужно будет только
	// сосредоточить фокус ввода на имеющейся строке, а не добавлять новую...
	if (currentIndex().isValid())
		setCurrentItem(currentItem(), QItemSelectionModel::Clear);
	iBas = rowCount();	// базовый индекс нового эл-та
	QTableWidgetItem *pi = InsertNewRow(iBas);
	openPersistentEditor(pi);
	setCurrentItem(pi, QItemSelectionModel::Select);
	m_sEditItem = "";
	m_state |= ADD;
}

/////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::OnDel отмечаю эл-т списка и строку в базе как удалённые
///
void CList::OnDel()
{
	int iRow = currentRow(), n;
	if (iRow < 0 || iRow >= rowCount())
		return;
	QBrush b = TextBrush(ITM_DEL);
	verticalHeaderItem(iRow)->setForeground(b);
	b.setColor(Qt::lightGray);
	item(iRow, 0)->setForeground(b);
	fl::CString *ps = fl::strs.static_obj(nullptr);
	int32_t iStr = BaseIndexCI();
	fl::a::COpener<fl::CStrings> os(fl::strs); // одной транзакцыей...
	do {
		iStr = fl::strs.Invalidate(fl::ioffset, iStr);
	} while (fl::strs.GetItem(fl::ioffset, iStr, *ps).IsValid());
	m_sEditItem = "";
	m_state &= ~(ADD|EDIT);
}

void CList::OnRem()
{
	int32_t iStr = BaseIndexCI(), n;
	if (iStr < 0)
		return;
	iStr = fl::strs.Rem(fl::ioffset, iStr);
	if (m_state & (SORTASC|SORTDES)) {
		m_state |= SORTASC|SORTDES;// сортирую по файловому отступу (по-умолчанию)
		OnHorisontalHeaderClicked(0);
	}
	DelRow(iStr);
	if ((n = rowCount()) > 0)
		setCurrentItem(item(MIN(iStr, n - 1), 0), QItemSelectionModel::ClearAndSelect);
	// нужно привести в соответствие знач.верт.заголовков с базовым индексом
	for (; iStr < n; iStr++)
		verticalHeaderItem(iStr)->setText(QString::number(iStr, 10));
	m_sEditItem = "";
	m_state &= ~(ADD|EDIT);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::OnReadTextFile читаю текстовый файл построчно. Строки из читаемого файла соответствуют
/// индексам из SI_... из common/strdef.h
///
void CList::OnReadTextFile()
{	// осторожно, тут может быть всё к ебеням
	struct CFlOpnr {
		FILE *pf;
		CFlOpnr(const char *pcsz) {
			pf = fopen(pcsz, "r");
		}
		~CFlOpnr() {
			if (pf)
				fclose(pf);
		}
	};
	int32_t n, i, nListRows = rowCount(), iInd;
	glb::CFxString spath;
	try {
		if (nListRows > 0 &&
		    QMessageBox::warning(this, tr(S_DLG_WRNG_CAPT), tr(S_DLG_WRNG_BODY),
		                         QMessageBox::No | QMessageBox::Yes, QMessageBox::No)
		        == QMessageBox::No)
			return;
		// выбрать файл
		QString qs = QFileDialog::getOpenFileName(this, tr(S_DLGOPNFL_CAPT), "~/", tr(S_DLGOPNFL_FLTR));
		if (!qs.length())
			return;
		spath = qPrintable(qs);
		// открыть файл
		CFlOpnr fl((const char*) spath);
		if (!fl.pf) {
			n = cerror; // ошибка чтения файла
			throw err::CCException(n, S_READ_FILE, time(NULL), n, glb::FileName(qPrintable(qs)));
		}// всё из файлового массива к ебеням
		fl::a::COpener<fl::CStrings> opnr(fl::strs);
		fl::strs.Empty();
		// всё из списка к ебеням.. а можно и поменять
		// читать файл построчно
		fl::CString s;
		s.m_str.SetCount(512);
		char *pc = (char*) (BYTE*) s.m_str;
		for (i = 0; fgets(pc, 512, fl.pf); i++) {
			n = strlen(pc);
			while (n > 0 && (isspace(pc[n - 1]) || pc[n - 1] == '\0'))
				n--;
			s.m_str.SetCountND(n);
			s.m_flags = 0;
			if (n && s.m_str.Last() == '*') {
				s.m_flags = ITM_SPW;
				s.m_str.SetCountND(--n);
			}
			if (!n) {
				QMessageBox::critical(this, tr(S_DLGCRTL_CAPT), tr(S_DLGCRTL_BODY));
				fl::strs.Empty();
				return;
			}
			s.m_tmc = s.m_tme = time(nullptr);
			if ((iInd = fl::strs.AddUnic(s)) < 0) {
				QMessageBox::critical(this, tr("Ошибка в файле"), tr("В файле \"%1\" нарушение целостности индекса %2 "\
						"в строке %3.").arg(tr(glb::FileName(spath))).arg((fl::CString::EStrIndices) (iInd * -1 - 1)).
																  arg(i + 1));
				return;
			}
			if (s.m_nref > 1) { // все строки уникальные
				glb::CFxString str;
				str.AddFrmt(255, S_NOT_UNIC, i + 1, glb::FileName(spath),
							fl::strs.BaseIndex(fl::CString::iStrsLeti,
											   fl::strs.FindEqual(s, fl::CString::iStrsLeti)) + 1);
				QMessageBox::critical(this, tr("Ошибка чтения файла"), (CCHAR*) str);
				fl::strs.Empty();
				return;
			}
			if (i < nListRows)
				SetRow(i, s);
			else
				AddNewRow(s);
		}
		if (!feof(fl.pf)) {
			n = cerror; // ошибка чтения файла
			throw err::CCException(n, S_READ_FILE, time(NULL), n, glb::FileName(qPrintable(qs)));
		}
		for (; i < nListRows; nListRows--)
			DelRow(i);
		if (m_state & (SORTASC|SORTDES)) {
			m_state &= ~(SORTASC|SORTDES);
			horizontalHeader()->setSortIndicatorShown(false);
		}
	} catch (err::CCException e) {
		char ac[256];
		snprintf(ac, 256, S_READ_FL_CATCH, e.text());
		QMessageBox::critical(this, tr(S_DLGCRTL_CAPT), tr(ac));
	} catch (...) {
		QMessageBox::critical(this, tr(S_DLGCRTL_CAPT), tr(S_UNKNOWN_EXCPTN));
	}
}

void CList::DelRow(int32_t iRow)
{
	delete takeItem(iRow, 0);
	delete takeVerticalHeaderItem(iRow);
	removeRow(iRow);
}

void CList::Empty()
{
	for (int i = rowCount() - 1; i >= 0; i--)
		DelRow(i);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::event для подготовки просмотра, изменения и добавления строк в список. Такой как чтение
/// имеющихся строк из базы и определения ширины столбца с текстом.
/// \param e: событие передаётся базовому классу, а тут выбирается подходящее для начала.
/// \return
///
bool CList::event(QEvent *e)
{
	QEvent::Type et = e->type();
	QTableWidgetItem *pi = NULL;
	const fl::CString *psf;
	try {// иницыацыя представления
		if (m_state == NONE && et == QEvent::WindowActivate) {
			// чтение известных строк в fl::strs и вывод в представление.
			QHeaderView *ph = verticalHeader();
			ph->setDefaultAlignment(Qt::AlignCenter);
			ph->setSectionResizeMode(QHeaderView::ResizeToContents);
			insertColumn(0);
			setHorizontalHeaderItem(0, new QTableWidgetItem("Строка"));
			ph = horizontalHeader();
			QMetaObject::Connection c =
					connect(ph, SIGNAL(sectionClicked(int)), this, SLOT(OnHorisontalHeaderClicked(int)));
			ASSERT((bool) c == true);
			fl::a::COpener<fl::CStrings> opnr(fl::strs);
			fl::CString *ps = fl::strs.static_obj(NULL);
			for (int i = 0, n = fl::strs.Count(fl::ioffset); i < n; i++)
				AddNewRow(fl::strs.GetItem(fl::ioffset, i, *ps));
			m_state |= ACTIVE; // только чтобы не нуль, not NONE
		}// далее залипуха - в предыдущем событии ещё не определена ширина вертикальных заголовка и полосы
		// прокрутки. Это происходит в недрах Qt где-то после первого WindowActivate; во время Paint уже известны.
		else if ((m_state & (ACTIVE|INIT)) == ACTIVE && et == QEvent::Paint) {
			QHeaderView *ph = verticalHeader();
			QScrollBar *psb = verticalScrollBar();
			int nw = width(), nVh = ph->width(), nSb = psb->width();
			if (nSb > nVh * 3)
				nSb = 0;//
			ph = horizontalHeader();
			ph->resizeSection(0, nw - nVh - nSb);
			ph->setSectionsClickable(true);
			m_state |= INIT;
		}
	} catch (err::CLocalException e) {
		if (!(m_state | INIT))
			throw;
		err::log.Add("%t исключение %d в ф-ции CList::event.", time(NULL), e.m_iStrID);
	} catch (err::CCException e) {
		if (!(m_state | INIT))
			throw;
		err::log.Add("%t С-исключение %s в ф-ции CList::event", time(NULL), e.text());
	} catch (...) {
		if (!(m_state | INIT))
			throw;
		err::log.Add("%t неизвестное исключение в ф-ции CList::event", time(nullptr));
	}
	return QTableWidget::event(e);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::focusInEvent происходит во время получения списком фокуса ввода. Два дня
/// убил на поиск этого события. Всё время ковырял в главном окне. Список получает это
/// событие, но не передаёт родительскому окну. А QListWidget передаёт, с ним проблем не было.
/// Пришлось делать "promotion" (так в Qt называется назначение своего класса для эл-та
/// управления в форме), точнее "promotion to" в этот класс, чтобы получить Esc в списке.
/// Однако, саму клавишу список от редактора не получает, зато редактор передаёт фокус в
/// список и текст, кстати сказать, тоже, только в том случае, если он был изменён п-лем.
/// Таким образом, в эту ф-цию напрямую попадает ESC, а ENTER опосредовано, при закрытии
/// редактора внутри в keyPressEvent. Тогда закрывать редактор тут не нужно.
/// \param event: здесь интересует "другое", а именно Esc в текстовом редакторе.
///
void CList::focusInEvent(QFocusEvent *event)
{
	if (m_state & (ADD|EDIT)) {
		if (!(m_state & INCLOSE))
			CloseEditor();
		if (m_state & ADD) { // не состоявшийся ввод новой строки. Удаляю её в списке, в базе её нет
			int c = currentRow(), n;
			DelRow(c);
			if ((n = rowCount()) > 0)
				setCurrentItem(item(MAX(c, n - 1), 0), QItemSelectionModel::ClearAndSelect);
		} else {	// отмена редактирования. Возвращаю прежний текст.
			QTableWidgetItem *pi = currentItem();
			if (pi->text() != m_sEditItem) {
				pi->setText(m_sEditItem);
				m_sEditItem.resize(0);
			}
		}
		m_state &= ~(ADD|EDIT); // редактирование завершено, редактор закрыт.
	}
	QTableWidget::focusInEvent(event);
}

//////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::keyPressEvent здесь фиксируется редактирование п-лем новой строки, или
/// имеющейся, в зависимости от состояния m_state. Фиксация в ответ на "ввод". В QListWidget
/// сюда просачивался Esc (отмена), но в этом списке его нет. Поэтому пришлось ловить фокус
/// в focusInEvent().
/// \param event
///
void CList::keyPressEvent(QKeyEvent *event)
{	// сначала ф-ция базового класса
	QTableWidget::keyPressEvent(event);
	Qt::Key key = (Qt::Key) event->key();
	int32_t n = rowCount(), c, iBas;
	// кнопки "домой" и "конец" базовый класс игнорирует
	if (key == Qt::Key_End) {
		if (n > 0 && !(m_state & (EDIT|ADD)) && n - currentIndex().row() > 1)
			setCurrentItem(item(n - 1, 0));
		return;
	}
	if (key == Qt::Key_Home) {
		if (n > 0 && !(m_state & (EDIT|ADD)) && currentIndex().row() > 0)
			setCurrentItem(item(0, 0));
		return;
	}
	if (key != Qt::Key_Return)
		return;
	// Редактор тут называется persistent потому что не закрывается сам даже после нажатия
	// п-лем Esc или Enter. Поэтому здесь он на месте. Кроме того, выяснено, что текст из
	// редактора в список передаётся автоматически во время передачи фокуса от редактора в
	// список, что происходит перед закрытием редактора или в ответ на Esc п-ля. На нажатие
	// п-лем Enter фокус списку не передаётся. Т.е. в списке старый текст, в редакторе - новый.
	iBas = BaseIndexCI();		// базовый индекс акивного (выделенного) эл-та
	QTableWidgetItem *pi = currentItem();
	if (iBas < 0 || !pi)
		return;
	QLineEdit *pe;
	QByteArray qab = pi->text().toUtf8();
	fl::CString *ps = fl::strs.static_obj(nullptr), s;
    n = qab.length();
	s.m_str.SetCountND(n);
	s.m_str.Edit(0, (CBYTE*) qab.data(), n);// ещё не изменённый редактором текст
    if (m_state & EDIT) {
        pe = FindEdit();				// при закрытии редактора текст эл-та меняется на новый
		q2s(pe->text(), s.m_str);
		if (!(s.m_str.GetCount())) {	// новый текст, его ещё нет в списке
			CloseEditor();				// вызывается focusEvent, эл-ту списка возвращается прежний текст
			OnDel();					// отмечаю строку в списке как удалённую
			ASSERT(rowCount() == fl::strs.Count());
			return;
		}
		m_state &= ~EDIT;				// редактирование строки списка завершено
		m_sEditItem = "";
		CloseEditor();
		pi->setForeground(QBrush());
        verticalHeaderItem(pi->row())->setForeground(QBrush());
		fl::strs.GetItem(fl::ioffset, iBas, *ps);
		s.m_flags = ps->m_flags & ~ITM_DEL;// флаги сохраняю (например, ITM_SPW)
		s.m_tmc = ps->m_tmc;
		s.m_tme = time(nullptr);
		fl::strs.EditUnic(s, fl::ioffset, iBas);// если эл-т iBas был удалён, теперь он действительный
	} else if (m_state & ADD) {			// здесь известно, что строку не редактирую как в сл.изменения недействит.строки
		ASSERT(!m_sEditItem.length());	// ...а добавляю новую в конец не отсортированного списка
        pe = FindEdit();				// при закрытии редактора текст эл-та меняется на новый
		q2s(pe->text(), s.m_str);		// этого текста ещё нет в списке, он упадёт туда при закрытии редактора
		// (debug) в focusInEvent нельзя удалять строку из списка потому что CloseEditor обращается к эл-ту,
		// который удаляется в focusInEvent. Ошибка обращения к памяти обнаружена при отладке.
		m_state &= ~ADD;
        CloseEditor();
		if (s.m_str.GetCount())	{
			fl::a::COpener<fl::CStrings> opnr(fl::strs);
			n = fl::strs.Count();
			s.m_tmc = s.m_tme = time(nullptr);
			iBas = fl::strs.AddUnic(s);
			if (n == fl::strs.Count()) {// если такая строка уже есть в массиве, дублировать её не нужно
				opnr.Close();
				ASSERT(rowCount() - n == 1);
				DelRow(n);
				if (m_state & (SORTASC|SORTDES)) {
					m_state |= SORTASC|SORTDES;// сортирую по файловому отступу (по-умолчанию)
					OnHorisontalHeaderClicked(0);
				}
				setCurrentItem(item(iBas, 0), QItemSelectionModel::ClearAndSelect);
			}
		} else {						// если строка пустая, удалю эл-т списка
			c = currentRow();
			DelRow(c);
			if ((n = rowCount()) > 0)
				setCurrentItem(item(MAX(c, n - 1), 0), QItemSelectionModel::ClearAndSelect);
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// \brief CList::resizeEvent подтянет ширину столбца с текстом в пределах между заголовками строки и
/// вертикальной полосой прокрутки.
/// \param event передаётся в событие родительского класса.
///
void CList::resizeEvent(QResizeEvent *event)
{
	QTableWidget::resizeEvent(event);
	QHeaderView *phh = horizontalHeader(), *pvh = verticalHeader();
	QScrollBar *psb = verticalScrollBar();
	int nw = width(), nh = pvh->width(), nsb = psb->width();
	if (nsb > nh * 3)
		nsb = 0;// если прокрутка не отображена, её ширина какого-то хуя 100.
	phh->resizeSection(0, nw - nh - nsb);
}

}// namespace ui