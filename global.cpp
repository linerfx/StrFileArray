/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл global.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или изменять
 * на условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения; либо версии 3 лицензии,
 * либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в надежде, что они будут
 * полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * global.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with each of
 * reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "global.h"
#include "../../common/FileMan.h"
#include "../../common/io_strings.h"
#include "strdef.h"
#include "../../common/ErrLog.h"
#include "../../common/io_path.h"
#include <QApplication>

namespace fl {
	CFileMan man;
	CStrings strs;
namespace a {
	CIndexIntegrity ii;
}// namespace a
	thread_local BYTES ab;
}
namespace glb {
    thread_local CFxString sAppPath;
	QApplication *g_pa;
	EInit g_init;
}
namespace err {// журнал (ошибок в том числе) для каждого потока
    CLog log;
///////////////////////////////////////////////////////////////////////////////////////////
/// \brief CLogFilePath ф-ция обратного вызова для журнала err::CLog
/// \return путь к файлу журнала
///
const char* CLog::PathName(glb::CFxString &sp) const
{
	const int cfn = 16;
	char pszfn[cfn];
	sp = glb::sAppPath;
	fl::CPath::AddName(sp, S_ELOG);
	time_t ntm = time(NULL);
	struct tm *pstm = localtime(&ntm);
	strftime(pszfn, cfn, "lg-%Y-%j.txt", pstm);	// год и день lgYYYY-jjj.txt 13 симолов, включая нуль.
	return fl::CPath::AddName(sp, pszfn);
}

}// namespace err
namespace glb {// путь к хранилищу приложения формируется при инициализации

//////////////////////////////////////////////////////////////////////////////////
/// \brief on_abort исполняю если процесс завершается сл.ошибкой:
/// \param nSig: SIGABRT, SIGFPE, SIGILL, SIGINT, SIGSEGV, SIGTERM
/// см. main()
///
void on_abort(int /*nSig*/)
{
	fl::strs.drop();
	fl::man.Destroy();
}

}// namespace glb