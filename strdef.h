#pragma once
/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл strdef.h — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или изменять
 * на условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения; либо версии 3 лицензии,
 * либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в надежде, что они будут
 * полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
 * или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * strdef.h is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with each of
 * reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
// ёбаная канитель с ёбаными строками.... в этом файле должны быть сука строки для
// ёбаных исключений, которые объявлены в ёбаном common
#include "../../common/strdef.h"

/////////////////////////////////////////////////////////////////////////////
/// определения строк, которые нужны до открытия файлового массива строк, которй
/// содержит все строки, используемые в приложении.
///
// имя программы - имя дирректории в системном хранилище для приложений
#define S_APPNM				"iTog"
#define S_ELOG				"журнал"
// ф-ция CList::OnReadTextFile строки диалога
#define S_DLG_WRNG_CAPT		"Редактор строк iTog"
#define S_DLG_WRNG_BODY		"Все строки в списке будут удалены.\nОткрываемый файл должен содержать 8-ми битные "\
	                        "Unicode символы.\nХотите продолжить?"
#define S_DLGOPNFL_CAPT		"Открой файл со строками iTog"
#define S_DLGOPNFL_FLTR		"Текстовые файлы (*.txt)"
#define S_DLGCRTL_CAPT		"Ошибка ввода строк iTog"
#define S_DLGCRTL_BODY		"Удали пустые строки из текстового файла и повтори попытку"
#define S_NOT_UNIC			"Строка №%d в файле %s не уникальная, повторяет строку №%d."
// формат для исключения "ошибка чтения файла"
#define S_READ_FILE			"%t ошибка (%d) чтения файла %s."
#define S_READ_FL_CATCH		"Ошибка чтения файла: %s."
#define S_UNKNOWN_EXCPTN	"Неизвестное исключение во время чтения файла"

/////////////////////////////////////////////////////////////////////////////
/// идентификаторы предопределённых строк. Значение каждого определения
/// соответствует индексу в файловом массиве S_STRAF.
///
