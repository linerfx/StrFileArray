/*
 * Copyright 2022 Николай Вамбольдт
 *
 * Файл mainwindow.cpp — часть программ reader, linerfx, iTog, iTogClient,
 * StrArrayReader.
 *
 * Указанные программы свободные: вы можете перераспространять их и/или изменять
 * на условиях Стандартной общественной лицензии GNU в том виде, в каком она была
 * опубликована Фондом свободного программного обеспечения; либо версии 3 лицензии,
 * либо (по вашему выбору) любой более поздней версии.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader распространяются в надежде, что
 * они будут полезными, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО
 * ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной
 * лицензии GNU.
 *
 * Копия Стандартной общественной лицензии GNU находится в файле LICENCE
 * вместе с программами. Если это не так, см. <https://www.gnu.org/licenses/>.
 *
 * en:
 * Copyright 2021 Nikolay Vamboldt
 *
 * mainwindow.cpp is part of reader, linerfx, iTog, iTogClient, StrArrayReader.
 *
 * These programms are free software: you can redistribute it and/or modify
 * them under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * reader, linerfx, iTog, iTogClient, StrArrayReader are distributed in the hope that
 * it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * The copy of the GNU General Public License is in file LICENCE along with each of
 * reader, linerfx, iTog, iTogClient, StrArrayReader. If not, see
 * <https://www.gnu.org/licenses/>.
 */
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "assert.h"
#include <QKeyEvent>
#include "strdef.h"
#include <QLineEdit>
#include "../../common/io_path.h"
#include "../../common/io_strings.h"
#include "../../common/exception.h"
#include "../../common/ErrLog.h"
#include "global.h"

namespace glb {
    extern thread_local CFxString sAppPath;
	extern EInit g_init;
	extern QApplication *g_pa;
}
namespace fl {
	extern CStrings strs;
}
namespace err {
	extern CLog log;
}
namespace ui {
////////////////////////////////////////////////////////////////////////////////////
/// \brief CMainWindow::CMainWindow конструктор по-умолчанию
/// \param parent
///
CMainWindow::CMainWindow(QWidget *parent) // parent = nullptr
	: QMainWindow(parent)
	, ui(new Ui::CMainWindow)
{
    try {
        ui->setupUi(this);
		QMetaObject::Connection c(connect(glb::g_pa, SIGNAL(aboutToQuit()), this, SLOT(OnExit())));
        ASSERT((bool) c == true);
        m_state = valid;
    } catch (err::CLocalException e) {
		if (glb::g_init != done)
			err::log.Add("%t местное исключение %d в %s стр. %d", time(nullptr), e.m_iStrID, glb::FileName(__FILE__), __LINE__);
		else {
			fl::CString *psa = fl::strs.static_obj(NULL);
			err::log.Add(SZ(SI_LE, *psa), time(nullptr), e.m_iStrID, glb::FileName(__FILE__), __LINE__);
		}
    } catch (err::CCException e) {
		if (glb::g_init != done)
			err::log.Add(S_IE, time(nullptr), e.text(), e.m_errno, glb::FileName(__FILE__), __LINE__);
		else {
			fl::CString *ps = fl::strs.static_obj(NULL);
			err::log.Add(SZ(SI_CE, *ps), time(nullptr), e.text(), glb::FileName(__FILE__), __LINE__);
		}
		err::log.Add(S_IEU, time(nullptr), glb::FileName(__FILE__), __LINE__);
    } catch (...) {
		err::log.Add(S_IEU, time(nullptr), glb::FileName(__FILE__), __LINE__);
    }
}

CMainWindow::~CMainWindow()
{
	delete ui;
}

CList* CMainWindow::FindList()
{
	CList *pv = findChild<CList*>("tableWidget");
	if (!pv) {
		ASSERT(false);
		throw err::CLocalException(SI_NED, time(NULL), glb::FileName(__FILE__), __LINE__);
	}
	return pv;
}

bool CMainWindow::event(QEvent *e)
{
	if (glb::g_init == none && !glb::sAppPath.GetCount()) try {
		glb::g_init = inproc;
		fl::CPath::CreateAppPath(S_APPNM);
		glb::CFxString s(glb::sAppPath);
//		int n = s.GetCount();
		// только папка со строками вероятно, создаётся новая.
		fl::CPath::AddName(s, S_STRAF);
		fl::CPath::CheckMakeDir(s);
/*		s.SetCountND(n);
		CPath::AddName(s, _SP(SI_LOGD));
		CPath::CheckMakeDir(s);*/
		glb::g_init = done;
	} catch (...) {
		m_state = invalid;
		throw;
	}
	return QMainWindow::event(e);
}

void CMainWindow::on_Edit_triggered()
{
	CList *pv = FindList();
	pv->OnEdit();
}

void CMainWindow::on_New_triggered()
{
	CList *pv = FindList();
	pv->OnNew();
}

void CMainWindow::on_Del_triggered()
{
	CList *pv = FindList();
	pv->OnDel();
}

/////////////////////////////////////////////////////////////////
/// \brief CMainWindow::OnExit окончательна зачистка всего.
///
void CMainWindow::OnExit()
{	// delete в CFxArray по завершении QApplication::exec почему-то швыряет исключение, или asserts.
	// потому что была ошибка: CFxString::operator +=(const CFxString&)) записывала за пределы выделенной памяти.
	// зачистка выделенной памяти:
    //	glb::sAppPath.SetCount(0);
}

void CMainWindow::on_Rem_triggered()
{
	CList *pv = FindList();
	pv->OnRem();
}



void CMainWindow::on_action_triggered()
{
	CList *pv = FindList();
	pv->OnReadTextFile();
}

}// namespace ui